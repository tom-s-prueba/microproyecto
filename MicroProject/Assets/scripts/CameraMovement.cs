using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    //VARIABLES USE
    public Transform target;
    public float SmoothSpeed;
    public Vector3 CameraPosition;

    private void FixedUpdate()
    {
        Vector3 Dposition = target.position + CameraPosition;
        Vector3 Sposition = Vector3.Lerp(transform.position, Dposition, SmoothSpeed * Time.deltaTime);

        transform.position = Sposition; 
    }
}
